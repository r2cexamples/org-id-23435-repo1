import Dependencies.Versions._

name := "encryption"
exportJars := true
publishArtifact := true
crossScalaVersions := Seq(scalaVersion.value, "2.13.6")

// tests are failing during parallel execution
Test / parallelExecution := false

libraryDependencies ++= Seq(
  "joda-time"         % "joda-time"  % "2.10.2",
  "com.pauldijou"     %% "jwt-circe" % "4.3.0",
  "com.agoda.commons" %% "utils"     % "1.0",
  "com.agoda" %% "warts-misc" % "1.0",
  "org.scalatest"     %% "scalatest" % "3.2.9" % Test
)