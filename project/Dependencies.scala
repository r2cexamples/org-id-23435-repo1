import sbt._

object Dependencies {
  object Versions {
    val scalaTest = "3.2.9"
  }

  val scalaTest = "org.scalatest" %% "scalatest" % Versions.scalaTest % Test
}
